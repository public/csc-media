V_BIT_RATE=768k
A_BIT_RATE=128k

LOG="timings/$2-mpg.log"
TIC=`date +%s`

$ffmpeg -i "$1" $ffmpegopts -v -1 -y \
       -vcodec mpeg2video -b "$V_BIT_RATE" \
       -acodec mp2 -ac 1 -ab "$A_BIT_RATE" \
       "encodes/$2.mpg"

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG

