V_BIT_RATE=768k
A_BIT_RATE=128k

LOG="timings/$2-flv.log"
TIC=`date +%s`

$ffmpeg -i "$1" $ffmpegopts -v -1 -y \
       -f flv -b "$V_BIT_RATE" \
       -ac 1 -ab "$A_BIT_RATE" \
       "encodes/$2.flv"

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG
