#!/bin/bash

export ffmpeg=/users/pbarfuss/ffmpeg2

if [ $# !=  "3" ] ; then
    echo "Usage: cut-mts input.mts start-point cut-length";
    echo "  For example: cut-mts video.mts 00:00:00 00:10:00";
    exit 1;
fi

$ffmpeg -i "$1" -ss "$2" -t "$3" -vcodec copy -acodec copy -f mpegts "cut-$1"
