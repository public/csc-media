#!/bin/bash

#####
# TODO: b4taylor
# * Progress printing

export ffmpeg=/users/pbarfuss/ffmpeg
export ffmpegopts="$3"

if [ $# !=  "3" ] ; then
    echo "Usage: encode input-video.format output_prefix opts";
    exit 1;
fi

mkdir "encodes" 2> /dev/null
if [ ! -d "encodes" ] ; then
    echo "Could not create directory: encodes"
    exit 1
fi

mkdir "timings" 2> /dev/null
if [ ! -d "timings" ] ; then
    echo "Could not create directory: timing"
    exit 1
fi

TIC=`date +%s`

# Job1: avi (xvid, mp3)
# ./make-avi.sh $1 $2 &
# broken

# Job2: mp4 (h264, aac)
./make-mp4.sh $1 $2 &

# Job3: mpg (mpeg2, mp2)
# ./make-mpg.sh $1 $2 &
# worthless

# Job4: ogg (theora, vorbis)
# ./make-ogg.sh $1 $2 &
# broken

# Job5: flv
#./make-flv.sh $1 $2 &

wait

./make-thumbs.sh $1 $2

TOC=`date +%s`

LOG="$2.log"
for i in timings/$2*.log ; do
    echo $i >> $LOG
    cat $i >> $LOG
    rm $i
done

{
    echo "$1"
    echo "Total: `expr $TOC - $TIC`"
} >> $LOG

mv $LOG "timings"

