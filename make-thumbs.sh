V_BIT_RATE=768kb
A_BIT_RATE=128kb

#No logging for this script
#LOG=`mktemp`
#TIC=`date +%s`

$ffmpeg -itsoffset -20 -i $1 -y -v -1 \
       -f rawvideo \
       -vcodec mjpeg -vframes 1 -s 90x60 \
       -an \
       "encodes/$2-thumb-small.jpg" 2> /dev/null

$ffmpeg -itsoffset -20 -i $1 -y -v -1 \
       -f rawvideo \
       -vcodec mjpeg -vframes 1 -s 720x480 \
       -an \
       "encodes/$2-thumb-large.jpg" 2> /dev/null

#TOC=`date +%s`

#echo "`expr $TOC - $TIC`" >> $LOG
