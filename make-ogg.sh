V_BIT_RATE=768k
A_BIT_RATE=128k

LOG="timings/$2-mkv.log"
TIC=`date +%s`

$ffmpeg -i "$1" $ffmpegopts -y -v -1 \
       -f ogg \
       -b "$V_BIT_RATE" \
       -ac 1 -ab "$A_BIT_RATE" -aq 60 \
       "encodes/$2.ogg"

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG
