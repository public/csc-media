V_BIT_RATE=768k
A_BIT_RATE=128k

LOG="timings/$2-mp4.log"
TIC=`date +%s`

$ffmpeg -i $1 -v -1 -y $ffmpegopts -strict experimental \
       -pass 1 -threads 0 -y \
       -vcodec libx264 -f mp4 -b "$V_BIT_RATE"  -bt "$V_BIT_RATE" \
       -fpre ./libx264-slow_firstpass.ffpreset \
       -an \
       "encodes/$2-first.mp4"

$ffmpeg -i $1 -v -1 -y $ffmpegopts -strict experimental \
       -pass 2 -threads 0 \
       -vcodec libx264 -f mp4 -b "$V_BIT_RATE" -bt "$V_BIT_RATE" \
       -fpre libx264-slow.ffpreset \
       -acodec aac -ac 1 -ab "$A_BIT_RATE" \
       "encodes/$2.mp4"

mv "ffmpeg2pass-0.log" "$2-ffmpeg2pass-0.log"
mv "x264_2pass.log" "$2-x264_2pass.log"

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG

