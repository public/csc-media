V_BIT_RATE=768k
A_BIT_RATE=128k

LOG="timings/$2-avi.log"
TIC=`date +%s`

$ffmpeg -i $1 $ffmpegopts -v -1 -y \
       -vcodec libxvid -b "$V_BIT_RATE" \
       -acodec libmp3lame -ac 1 -ab "$A_BIT_RATE" \
       "encodes/$2.avi"

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG

